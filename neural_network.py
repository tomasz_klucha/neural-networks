import numpy as np

class NeuralNetwork():

    def __init__(self, layers_definition):
        self.__layers_definition = layers_definition
        self.__num_of_layers = len(layers_definition)
        self.__weights = []
        self.__biases = []
        
        self.__create_weights_and_biases()

    def __create_weights_and_biases(self):
        for i in range(1, len(self.__layers_definition)):
            a = self.__layers_definition[i - 1]
            b = self.__layers_definition[i]

            self.__weights.append(np.random.randn(a, b))
            self.__biases.append(np.random.randn(b).T)

    def train(self, inputs, expected_outputs, epochs, learning_rate):
        for i in range(epochs):

            err_sum = 0
            for j in range(inputs.shape[0]):
                inp = inputs[j].T
                eo = expected_outputs[j].T

                layers_sums, layers_activations = self.__feed_forward(inp)

                err = np.sum((eo - layers_activations[-1])**2)

                nabla_w, nabla_b = self.__back_propagation(eo, err, layers_sums, layers_activations)

                for k in range(self.__num_of_layers - 1):
                    self.__weights[k] = self.__weights[k] - (learning_rate * nabla_w[k])
                    self.__biases[k] = self.__biases[k] - (learning_rate * nabla_b[k])

                err_sum += err.real
            
            print('Error: ' + str(err_sum / inputs.shape[1]))
            #print('Epoch: ' + str(i))
    
    def __feed_forward(self, inp):
        
        last_activation = inp
        layers_sums = []
        layers_activations = [last_activation]
        for i in range(self.__num_of_layers - 1):
            w = self.__weights[i]
            b = self.__biases[i]

            t1 = np.dot(w.T, last_activation)
            t2 = np.add(t1, b)
            t3 = self.__activ_func(t2)

            layers_sums.append(t2)
            layers_activations.append(t3)

            last_activation = t3
        
        return layers_sums, layers_activations


    def __back_propagation(self, expected_output, err, layers_sums, layers_activations):
        nabla_w = [np.zeros(w.shape) for w in self.__weights]
        nabla_b = [np.zeros(b.shape) for b in self.__biases]
        
        # last layer first
        dCdq = self.__cost_deriv(layers_activations[-1], expected_output)
        dqdf = self.__activ_deriv(layers_sums[-1])
        dfdw = layers_activations[-2]

        delta = dCdq * dqdf

        nabla_w[-1] = np.outer(dfdw, delta)
        nabla_b[-1] = delta

        # now hidden layers
        for l in range(2, self.__num_of_layers):
            w = self.__weights[-l + 1]
            
            delta = np.dot(w, delta) * self.__activ_deriv(layers_sums[-l]) 

            nabla_w[-l] = np.outer(layers_activations[-l - 1], delta)
            nabla_b[-l] = delta


        return (nabla_w, nabla_b)


    def __activ_func(self, x):
        return 1 / (1 + np.exp(-x))

    def __activ_deriv(self, x):
        return self.__activ_func(x) * (1 - self.__activ_func(x))

    def __cost_deriv(self, out, exp_out):
        return out - exp_out

def main():
    nn = NeuralNetwork([3, 4, 4, 3])

    inputs = np.array([[0, 0, 0],
                       [0, 0, 1],
                       [0, 1, 0],
                       [0, 1, 1],
                       [1, 0, 0],
                       [1, 0, 1], 
                       [1, 1, 0],
                       [1, 1, 1]])
    
    expected_outputs = np.array([[0, 0, 1],
                                [0, 1, 0],
                                [0, 1, 1],
                                [1, 0, 0],
                                [1, 0, 1], 
                                [1, 1, 0],
                                [1, 1, 1],
                                [0, 0, 0]])

    nn.train(inputs, expected_outputs, 1000, 3.0)

    r = nn.test(np.array([0, 0, 0]))
    print(r)

    r = nn.test(np.array([1, 0, 0]))
    print(r)

if __name__ == '__main__':
    main()
